Converts a folder containing notes in `*.org` format into `*.html` files, preserving the same folder structure. 

## Usage

```sh 
$ org-to-html --css _css/template.css --input_dir ~/org-notes --output_dir ~/org-notes-html --photo_dir images/
```

