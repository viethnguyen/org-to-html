** Jul 17, 2019 
- Use =structopt= to get command line arguments 
- What is =std::path::PathBuf=? it's an owned, mutable path. 
- Probably need to look up some cookbook for common operations, such as file traverals, etc. 
- In the book "Rust standard library cookbook", it is recommended that I should use =walkdir= to traverse a directory. 
- What is the difference between =std::path::Path= and =std::path::PathBuf=?
- What is the =OsStr=? 
- I'm now able to print out all org files inside the input directory. 
- =walkdir::FilterEntry= is a recursive directory iterator that skips entries. Values of this type are created by calling =.filter_entry()= on an IntoIter, which is formed by calling =.into_iter()= on a WalkDir. 
- How to store the paths of the filtered org files into a collection? 
- I tried do it easy first, add them into a Vec. However, if I gradually add an item into the vector, the item is said to be not live long enough. 
- What is =filter_map=? 
- I found this link quite useful to explain the use of =filter_map=: http://xion.io/post/code/rust-iter-patterns.html
- With a simple =.filter_map(Result::ok), we can pass through a sequence of =Result=s and yield only a "successful" values. Remember that the Erros will be discarded by it. 

// Read all text lines from a file: 
let lines: Vec<_> = BufReader::new(fs::File::open("file.ext")?).lines().filter_map(Result::ok).collect();

- I find =walkdir= quite confusing, switch to use =glob= crate instead. 
- Problem now: how to construct a pattern to glob from a =PathBuf= and a =/*.org= extension? 
- Done, using =PathBuf= to push string to the end of =in_dir=. 

** Jul 18, 2019 
- Moved the routine of finding org files in the input dir into a separate function. 
- To do next: call pandoc on the list of org files 
- There is a =pandoc= crate which wraps the pandoc command line tool. 
- what is pandoc format? Let's skip. 
- When using pandoc API, had quite some problems with string conversion. Need a comprehensive review. 
- Next: need to convert an absolute path returned from =glob= to a relative path, in order to get the correct destination path. 
- *Question*: When writing a function that returns a FilePath, how should I declare a return type that include both error possibility? 
- =PathBuf= can be converted to =&str= using =to_str()=, but the return type is actually =Option<&str>=. 
- I found what I need: function =strip_prefix= of =PathBuf=. 
- Another useful function: =with_extension= of =PathBuf=. 
- Ok, I'm now able to call pandoc crate with correct input and output file paths. But problems remain: 1) If the destination directory doesn't exist, pandoc doesn't do any conversion (no error), 2) the CSS path need to be relative to the root folder (both in the source org folder and the destination html folder), 3) need to create a landing page =index.html= with the pointers to all articles. 
- Done with 2): Checked the CSS path to be relative to the root folder and the file should exist. 
- I need to find relative path to the css file from each org file, so I can call pandoc with correct css parameter (The path to the css file will be added to the output HTML folder). There is a crate called =pathdiff= offers this functionality. 
- Continued to do some other functionalities: create destination directories if not existed. 
- *Question*: I used too many =unwrap()=. Is it a good practice? 
- Done: Copy css file to destination. 
- To do: Create a landing page =index.html=! 
- *Note*: The way I'm writing code right now is too verbose. Take a look at this question: https://www.reddit.com/r/rust/comments/7mu7q1/is_working_with_paths_always_this_painful/
to see some guides on how to handle Path and PathBuf correctly. 

** Jul 19, 2019 
- There is a Rust crate for launching HTTP server from a directory: =simple-http-server=. But it still doesn't display the TITLE of each HTML file. I still want to use my own. 
- =horrowshow-rs=: a macro-based html templating library. https://github.com/Stebalien/horrorshow-rs
- So my next programming task would be: use =horrorshow= to build an index.html file that lists all the HTML files inside the output folder; each file should be displayed with the link to the actual file and its title. 
- Another problem: In some org files, there are some links to images, which need to be preserved! 

** Jul 22, 2019 
- Finished: copying image folder to destination with correct path. 
- Need to use =fs_extra= crate for copying the whole folder. 

** Jul 24, 2019 
- Working on creating the index page. 
- Need to use =horrorshow=. 
- Having trouble understanding how to traverse the paths found by glob and inject them into the html template. 
- Probably this =horrorshow= is difficult to use. Consider using =yew=? 
- Follow example in =horrorshow= repo here: https://github.com/Stebalien/horrorshow-rs/blob/master/examples/blog.rs
- Now need to convert results from =glob= to a vector of Article. OK, done. 
- Now need to get the title of the post from the html file. 
- Problem now is that I get all the paths from =glob= into one big collection, but then it's difficult to categorize the paths/articles into categories based on the folder! Probably using WalkDir is better? 

** July 25, 2019 
- Now trying to extract a page title given an HTML document.
- Will use =select.rs= for this task. 
- Code for creating a document to be used with select.rs: 

    let f = fs::File::open(html_path).unwrap();
    let document = Document::from_read(f).unwrap();
    let temp: Vec<_> = document.find(Name("title")).collect();

- Next step: Use WalkDir to divide articles into categories. Maybe write a HTML render function that can render the list of articles in the same folder? 

** July 26, 2019 
- Plan to categorize the index page using WalkDir. 
- Let's try to write all code, including travering using WalkDir, inside the =render= function.
- I tried, but unable to merge rust code with the horrorshow's marco code! So I give up here. 
- Will try another HTML templating crate. 
- =handlerbars=, =markup=
- Let's try =markup=! https://crates.io/crates/markup
- Macros in =markup= are much easier to use! 
- Done displaying the notes in correct folder order! 
- Next step: better error handling. Use fewer =unwrap()=! Then document the project (usage, assumptions) carefully in README.md 

** July 29, 2019 

- Trying to do error handling more properly. 
- The preferred way to handle errors would be to use =failure= crate. It documentation: https://boats.gitlab.io/failure/
- There are =upwrap_or()= and =unwrap_or_else()= if it makes sense to use a default value when you get a =None=.
- There's also =ok_or()= for converting an =Option= into a =Result= which you can then use ? tn to return early. 
- Don't unwrap. An easy alternative is to use the ? operator and Result<..., Box<dyn Error>> as function's return type. Box<dyn Error>> is the most generic error type, so most error types are compatible with it and you don't have to implement code for conversions yourself. 
- Can use =error-chain= crate: https://brson.github.io/2016/11/30/starting-with-error-chain
- Let's try to use =failure=! 
- Need to use =failure_derive= too! 
- The failure crate suggests 2 data types: =Fail= and =Error=. 
- I'm trying to make my own enum =MyError=, which derives traits Display and Fail. 
- I'm now setting the main function to return std::io::Result<()>. My helper function will return my custom MyError type. 
- How to convert an Option to a Result? --> using =ok_or()=. 
- One of my function call pandoc execution, which produces its own type of error. What to do? The =failure= crate suggests using the =Error= type! 
- I'm keeping things simple: Let's only use a custom Fail type for now. 
- Done with error handling.
- Next thing to do: Each time running the tool, it should delete the old output folder. 
- How about experimenting with multithreaded programming? 
