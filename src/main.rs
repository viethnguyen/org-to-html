#[macro_use] extern crate failure;
#[macro_use] extern crate failure_derive;

use glob::glob;
use select::document::Document;
use select::predicate::{Name};
use std::path::PathBuf;
use std::fs::{self};
use structopt::StructOpt;
use pandoc::{Pandoc};
use failure::Error;

#[derive(Debug, StructOpt)]
#[structopt(name="example", about="An example of StructOpt usage.")]
struct Opt{
    #[structopt(short = "i", long = "input_dir")]
    inputdir: PathBuf,
    #[structopt(short = "o", long = "output_dir")]
    outputdir: PathBuf,
    #[structopt(short = "c", long = "css")]
    csspath: PathBuf,
    #[structopt(short = "p", long = "photo_dir")]
    photodir: PathBuf, 
}

#[derive(Fail, Debug)]
pub enum MyError{   
    #[fail(display="Both the css file and input dir should be relative to the input dir and they should exist!")]
    RelativeFileError(PathBuf),
    #[fail(display = "{}", _0)]
    IoError(#[cause] std::io::Error),    
    #[fail(display = "{}", _0)]
    PandocError(#[cause] pandoc::PandocError),
}

fn main() -> std::io::Result<()> {
    let args = Opt::from_args();
    let in_dir = args.inputdir;
    let out_dir = args.outputdir;
    let css_path = args.csspath; 
    let photo_dir = args.photodir;

    // clean up the destination first 
    /* if out_dir.exists() {
        fs::remove_dir_all(&out_dir).unwrap();
    } */
    fs::create_dir_all(&out_dir)?;        

    // precheck css file and photo directory
    match verify_relative_file(&css_path, &in_dir){
        Ok(_) => (),
        Err(e) => {
            println!("Problem with this path: {}", css_path.to_str().unwrap());
            println!("{}", e);
            return Ok(());
        }
    }
    match verify_relative_file(&photo_dir, &in_dir) {
        Ok(_) => (),
        Err(e) => {
            println!("Problem with this path: {}", photo_dir.to_str().unwrap());
            println!("{}", e);
            return Ok(());            
        }            
    }

    // copy the css file
    let mut css_path_abs = PathBuf::new();
    css_path_abs.push(&in_dir);
    css_path_abs.push(&css_path); 
    let mut dest_css_path_abs = PathBuf::new();
    dest_css_path_abs.push(&out_dir);
    dest_css_path_abs.push(&css_path);
    fs::create_dir_all(&dest_css_path_abs.parent().unwrap())?;
    fs::copy(&css_path_abs, &dest_css_path_abs)?;
    
    // get the list of org files 
    let org_file_paths = get_org_file_paths(&in_dir);
    for org_path in org_file_paths {
        if let Ok(org_path) = org_path{
            let html_path = get_html_path(&org_path, &in_dir, &out_dir);
            fs::create_dir_all(&html_path.parent().unwrap())?;
            println!("Convert: `{}` --> `{}`", org_path.to_str().unwrap(), html_path.to_str().unwrap());
            match convert_org_file_to_html(&org_path, &html_path, &css_path_abs){
                Ok(_) => (),
                Err(e) => {
                    println!("Error while converting files...");
                    println!("{}", e);
                    return Ok(())
                }
            }
        };        
    }

    // copy image directory 
    let mut image_path_abs = PathBuf::new();
    image_path_abs.push(&in_dir);
    image_path_abs.push(&photo_dir); 
    let mut dest_image_path_abs = PathBuf::new();
    dest_image_path_abs.push(&out_dir);
    dest_image_path_abs.push(&photo_dir);
    if dest_image_path_abs.exists() {
        fs::remove_dir_all(&dest_image_path_abs)?;
    }    
    fs::create_dir_all(&dest_image_path_abs)?;
    let options = fs_extra::dir::CopyOptions::new(); //Initialize default values for CopyOptions
    match fs_extra::dir::copy(&image_path_abs, &(dest_image_path_abs.parent().unwrap()), &options){
        Ok(_) => println!("Image folder copied to: {}", dest_image_path_abs.to_str().unwrap()),
        Err(e) => println!("Error copying image: {:?}", e),
    };

    // Create index page 
    create_index_page_markup(&out_dir, &css_path)?;

    Ok(())
}

fn verify_relative_file(relative_path: &PathBuf, in_dir: &PathBuf) -> Result<(), MyError>{
    if relative_path.is_absolute(){
        return Err(MyError::RelativeFileError(PathBuf::from(relative_path)));
    }
    let mut temp_path = PathBuf::new();
    temp_path.push(in_dir);
    temp_path.push(relative_path);
    if !temp_path.exists(){
        return Err(MyError::RelativeFileError(PathBuf::from(relative_path)));
    }
    Ok(())
}

fn get_html_path(org_path: &PathBuf, in_dir: &PathBuf, outdir: &PathBuf) -> PathBuf{
    let relative_path = convert_absolute_to_relative_path(&in_dir, &org_path);
    let mut  html_path = PathBuf::new();
    html_path.push(outdir);
    html_path.push(relative_path.with_extension("html"));
    html_path
}

fn get_org_file_paths(in_dir: &PathBuf) -> Vec<Result<PathBuf, glob::GlobError>>{
    let mut glob_pattern = in_dir.clone();
    glob_pattern.push("**/*.org");
    let org_file_paths : Vec<_> = 
    glob(glob_pattern.to_str().unwrap())
    .expect("Failed")
    .collect();
    org_file_paths
}

fn convert_absolute_to_relative_path(root_dir: &PathBuf, abs_path: &PathBuf) -> PathBuf{
    if !abs_path.starts_with(root_dir){
        panic!("Root dir is not the parent of the absolute path!");
    }

    let relative_path = abs_path.strip_prefix(root_dir).unwrap();
    relative_path.to_path_buf()
}

fn convert_org_file_to_html(org_path: &PathBuf, html_path: &PathBuf, css_path_abs: &PathBuf) -> Result<pandoc::PandocOutput, MyError> { //-> Result<pandoc::PandocOutput, pandoc::PandocError>{
    let relative_path_to_css = pathdiff::diff_paths(&css_path_abs, &org_path.parent().unwrap()).unwrap();
    let mut pd = Pandoc::new();
    pd.set_input(pandoc::InputKind::Files(vec![org_path.clone()]));
    pd.set_output(pandoc::OutputKind::File(html_path.to_str().unwrap().to_string()));
    pd.add_option(pandoc::PandocOption::Css(relative_path_to_css.to_str().unwrap().to_string()));
    pd.add_option(pandoc::PandocOption::TableOfContents);
    pd.add_option(pandoc::PandocOption::Standalone);

    // run pandoc
    match pd.execute() {
        Ok(pd_output) => Ok(pd_output),
        Err(pd_error) => Err(MyError::PandocError(pd_error)),
    }
}

markup::define! {
        Render<'a>(output_dir: &'a PathBuf, css_path_relative: &'a PathBuf) {
            {markup::doctype()}
            html {
                head {
                    meta["http-equiv"="Content-Type", content="htext/html; charset=utf-8"];
                    meta["http-equiv"="Content-Style-Type", content="text/css"];                   
                    title { "My notes" }
                    link[rel="stylesheet", href={format!("{}", css_path_relative.to_str().unwrap())}, type="text/css"];
                }
                body {
                    #header {
                        h1 .title {
                            "Viet Nguyen's notes"
                        }
                    }
                    { let mut file_paths = Vec::new(); }
                    @ for entry in fs::read_dir(output_dir).unwrap(){     
                        { let entry = entry.unwrap(); }
                        { let is_dir = entry.path().is_dir(); }       
                        @ if {is_dir} {
                            { RenderFolder {output_dir: output_dir, root_dir: &entry.path() } }
                        } else {
                            { file_paths.push(entry.path()); }
                        }
                    }
                    h2{"----- Others -----"}
                    @ for path in {file_paths} {
                        
                        {let relative_path = convert_absolute_to_relative_path(&output_dir, &path);}                        
                        {let title = get_title(&path);}

                        {RenderLink { title: &title, link: &relative_path }}                            
                        br; 
                    }
                }
            }
        }
        RenderFolder<'a>(output_dir: &'a PathBuf, root_dir: &'a PathBuf){
            { let mut glob_pattern = PathBuf::from(root_dir.clone()); }
            { glob_pattern.push("*.html"); }
            { let html_file_paths : Vec<_> = 
                glob(glob_pattern.to_str().unwrap())    
                .expect("Failed")
                .collect(); }
            
            @ if {html_file_paths.len() > 0} {
                {let relative_path = convert_absolute_to_relative_path(&output_dir, &root_dir);}                                    
                h2{"#" { relative_path.to_str().unwrap() }}
                { RenderLinks {output_dir: output_dir, root_dir: root_dir }}
            }
        }
        RenderLink<'a>(title: &'a str, link: &'a PathBuf){
            a[href={format!("{}", link.to_str().unwrap())}] { {title} }
        }
        RenderLinks<'a>(output_dir: &'a PathBuf, root_dir: &'a PathBuf){
            { let mut glob_pattern = PathBuf::from(root_dir.clone()); }
            { glob_pattern.push("**/*.html"); }
            { let html_file_paths : Vec<_> = 
                glob(glob_pattern.to_str().unwrap())    
                .expect("Failed")
                .collect(); }
            ul{
                @ for path in {html_file_paths} {
                    li {
                        {let path = path.unwrap();}
                        {let relative_path = convert_absolute_to_relative_path(&output_dir, &path);}                        
                        {let title = get_title(&path);}
                        {RenderLink { title: &title, link: &relative_path }}
                    }                        
                }              
            }
        }
    }

fn create_index_page_markup(output_dir: &PathBuf, css_path_relative: &PathBuf) -> std::io::Result<()>{
    let mut index_file_path = PathBuf::new();
    index_file_path.push(&output_dir);
    index_file_path.push("index.html");

    let actual = Render { output_dir: output_dir, css_path_relative: css_path_relative }.to_string();
    fs::write(&index_file_path, actual)?;
    Ok(())
}

fn get_title(html_path: &PathBuf) -> String {
    let f = fs::File::open(html_path).unwrap();
    let document = Document::from_read(f).unwrap();
    let temp: Vec<_> = document.find(Name("title")).collect();
    let node = temp[0]; 
    let title = node.text();
    if !title.is_empty() {
        title
    }
    else {
        html_path.to_str().unwrap().to_owned()
    }
}